<!doctype html>
<html lang="es">
    <head>
        <title>
            Generar
        </title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
    </head>
    <body>
        <header>
            <article id="titulo">
                <h1>Procesos  de ingeniería de software</h1>
            </article>
        </header>
        <section id="contenido">
            <aside id="izquierda">
                <p>Oprima el botón y genere un <br>archivo de texto para luego leerlo</p> 
                <button id="generar" type="button" class="btn btn-success">Generar</button>
                <br>
            </aside>
            <section id="centro">
            </section>
        </section>
        <footer>         
        </footer>
    </body>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#generar").click(function(){
                var fso = new ActiveXObject("Scripting.FileSystemObject");
                var s = fso.CreateTextFile("C:\\test.txt", True);
                s.writeline("-----------------------------");
                s.writeline("Procesos");
                s.writeline("Roberto Navas");
                s.writeline("-----------------------------");
                s.Close();
            });
        });
    </script>
</html>